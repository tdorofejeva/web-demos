
function add(){
    let number1 = document.querySelector('#number1');
    let number2 = document.querySelector('#number2');
    let sum = parseInt(number1.value) + parseInt(number2.value);

    let result = document.querySelector('#result');
    result.innerHTML = sum;
}

function sub(){
    let number1 = document.querySelector('#number1');
    let number2 = document.querySelector('#number2');
    let minus = parseInt(number1.value) - parseInt(number2.value);

    let result = document.querySelector('#result');
    result.innerHTML = minus;
}

function mult(){
    let number1 = document.querySelector('#number1');
    let number2 = document.querySelector('#number2');
    let multiplic = parseInt(number1.value) * parseInt(number2.value);

    let result = document.querySelector('#result');
    result.innerHTML = multiplic;
}

function divide(){
    let number1 = document.querySelector('#number1');
    let number2 = document.querySelector('#number2');
    let divid = parseInt(number1.value) / parseInt(number2.value);

    let result = document.querySelector('#result');
    result.innerHTML = divid;
}

function clearCalc(){
    let number1 = document.querySelector('#number1');
    let number2 = document.querySelector('#number2');
    number1.value = "";
    number2.value = "";

    let result = document.querySelector('#result');
    result.innerHTML = "";
}